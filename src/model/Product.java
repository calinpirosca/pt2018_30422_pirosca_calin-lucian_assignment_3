package model;

public class Product {

	private String productID;
	private String price;
	private String amount;
	private String description;
	private String category;
	
	public String getProductID() {
		return productID;
	}
	public String getPrice() {
		return price;
	}
	public String getAmount() {
		return amount;
	}
	public String getDescription() {
		return description;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Product() {
	}
	
	public Product(String productID, String price, String amount, String description) {
		this.productID = productID;
		this.price = price;
		this.amount = amount;
		this.description = description;
	}
	
	public Product(String productID, String price, String amount, String description, String category) {
		this.productID = productID;
		this.price = price;
		this.amount = amount;
		this.description = description;
		this.category = category;
	}
	
	@Override
	public String toString() {
		return "id: " + productID + "\n" + "name : " + description + "\namount : " + amount + "\nprice : " + price + "\ncategory : " + category + "\n";
	}

}
