package model;

/**
 * 
 * Class used as a blueprint for each client existing in the database.
 * 
 * @author Pirosca Calin Lucian
 *
 */

public class Client {
	private String clientID;
	private String address;
	private String lastName;
	private String firstName;
	
	public String getClientID() {
		return clientID;
	}
	public String getLastName() {
		return lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getAddress() {
		return address;
	}
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public Client() {
	}
	
	public Client(String nume, String prenume, String CNP, String adresa) {
		this.clientID = CNP;
		this.lastName = nume;
		this.firstName = prenume;
		this.address = adresa;
	}
	
	@Override
	public String toString() {
		return "id: " + clientID + "\n" + "name : " + firstName + " " + lastName + "\nAddress : " + address + "\n\n";
	}
	
}
