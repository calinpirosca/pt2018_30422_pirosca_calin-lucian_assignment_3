package model;

/**
 * Class used as a blueprint for each bill that'd be printed by the application.
 * Each product will have a different bill.
 * 
 * @author Pirosca Calin Lucian
 *
 */

public class Bill {

	private String description;
	private String units;
	private String pricePerUnit;
	private String total;
	public String getDescription() {
		return description;
	}
	public String getUnits() {
		return units;
	}
	public String getPricePerUnit() {
		return pricePerUnit;
	}
	public String getTotal() {
		return total;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setUnits(String units) {
		this.units = units;
	}
	public void setPricePerUnit(String pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public Bill(String description, String units, String pricePerUnit, String total) {
		this.description = description;
		this.units = units;
		this.pricePerUnit = pricePerUnit;
		this.total = total;
	}
	
	@Override
	public String toString() {
		return "Description " + description + " Quantity " + units + " Price/Unit " + pricePerUnit + " Total " + total;
	}
	
}
