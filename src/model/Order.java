package model;

/**
 * 
 * Class used as a blueprint for each order issued when a customer buys some products. It also says
 * which user ordered the products and when.
 * 
 * @author Pirosca Calin Lucian
 *
 */
public class Order {

	private String orderID;
	private String clientID;
	private String date;
	private int orderPrice;
	
	public String getOrderID() {
		return orderID;
	}

	public String getClientID() {
		return clientID;
	}

	public String getDate() {
		return date;
	}

	public int getOrderPrice() {
		return orderPrice;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setOrderPrice(int orderPrice) {
		this.orderPrice = orderPrice;
	}

	public Order() {
	}
	
	public Order(String orderID,String date, String clientID){
		this.orderID = orderID;
		this.clientID = clientID;
		this.date = date;
	}
	
	public Order(String orderID,String date, String clientID,int orderPrice){
		this.orderID = orderID;
		this.clientID = clientID;
		this.orderPrice = orderPrice;
		this.date = date;
	}
	
	@Override
	public String toString() {
		return "orderID " + orderID + " clientID " + clientID + " orderPrice " + orderPrice + " date " + date;
	}
}
