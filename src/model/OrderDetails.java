package model;
/**
 * 
 * Class used as a blueprint to be mapped in the database. It is related to the order and specifies the products
 * that were ordered and their quantity and price.
 * 
 * @author Pirosca Calin Lucian
 *
 */
public class OrderDetails {

	private int orderDetailsID;
	private int productID;
	private int productQuantity;
	private int productPrice;
	private int orderID;
	public int getOrderDetailsID() {
		return orderDetailsID;
	}
	public int getProductID() {
		return productID;
	}
	public int getProductQuantity() {
		return productQuantity;
	}
	public int getProductPrice() {
		return productPrice;
	}
	public int getOrderID() {
		return orderID;
	}
	public void setOrderDetailsID(int orderDetailsID) {
		this.orderDetailsID = orderDetailsID;
	}
	public void setProductID(int productID) {
		this.productID = productID;
	}
	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}
	public void setProductPrice(int productPrice) {
		this.productPrice = productPrice;
	}
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	
	public OrderDetails() {
	}
	
	public OrderDetails(int orderDetailsID, int productID, int productQuantity, int productPrice, int orderID) {
		this.orderDetailsID = orderDetailsID;
		this.productID = productID;
		this.productQuantity = productQuantity;
		this.productPrice = productPrice;
		this.orderID = orderID;
	}
	
	
	
	
}
