package start;

import java.io.IOException;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * The main class that is called when the application begins
 * 
 *	@author Pirosca Calin Lucian
 */

public class Main extends Application {

	private final static String iconPath = "/external/iconTaskbar.png";

	/**
	 * Class that loads the main window that is displayed as soon as the application starts
	 * FXML and CSS files are loaded here and the icon and style is specified as well.
	 * 
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	
	@Override
	public void start(Stage primaryStage) throws IOException {
		primaryStage.setTitle("Queue Simulation");
		primaryStage.initStyle(StageStyle.TRANSPARENT); // to remove minimize,maximize,close button
		Parent root = FXMLLoader.load(getClass().getResource("/view/fxml/MainFXML.fxml"));
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("/view/css/application.css").toExternalForm());
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		setIcon(primaryStage); // this works even though stageStyle.Transparent to display the icon on the
								// windows taskbar.
		primaryStage.show();	
	}

	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * This sets the icon as the image given by the iconPath
	 * 
	 * @param stage			the stage to which to apply the given icon
	 * 
	 * @see Image
	 * 
	 */
	public static void setIcon(Stage stage) {
		Image icon = new Image(iconPath);
		stage.getIcons().add(icon);
	}
}
