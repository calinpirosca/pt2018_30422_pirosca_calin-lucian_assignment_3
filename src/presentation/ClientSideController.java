package presentation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.jfoenix.controls.JFXTextField;

import business.FXMLFactory;
import business.pdf.Invoice;
import database.Connector;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Bill;
import model.Order;
import model.OrderDetails;
import model.Product;

/**
 * Controller used for the ClientSide of the application
 * 
 * @author Pirosca Calin Lucian
 *
 */

public class ClientSideController {

	double x, y;

	List<Node> products = new ArrayList<Node>();
	List<Product> productList = new ArrayList<Product>();
	List<Node> buttons = new ArrayList<Node>();
	List<Bill> currentBill = new ArrayList<Bill>();

	@FXML
	VBox vBox, vBoxButtons;

	@FXML
	ScrollPane sp;

	@FXML
	JFXTextField cnp;

	@FXML
	AnchorPane mainWindow, content, productsPane;

	@FXML
	Label error;

	String clientID;
	
	/**
	 * 
	 * Method used to set the opacity back to 1 after the window was moved
	 * 
	 * @param event		Used to get the stage
	 */
	@FXML
	public void windowMoved(MouseEvent event) {
		((Stage) ((Node) event.getSource()).getScene().getWindow()).setOpacity(1);
	}
	/**
	 * Method used to move the window and set the opacity to 0.7 while moving it
	 * 
	 * @param event		Used to get the stage
	 */
	@FXML
	public void moveWindow(MouseEvent event) {
		Stage temp = (Stage) ((Node) event.getSource()).getScene().getWindow();
		temp.setX(event.getScreenX() - x);
		temp.setY(event.getScreenY() - y);
		temp.setOpacity(0.7);
	}
	/**
	 * 
	 * Prerequisite method in order to be able to actually move the window when dragged
	 * Storing the initial position
	 * 
	 * @param event		Used to get the current position of the window
	 */
	@FXML
	public void moveWindowPre(MouseEvent event) { // prerequisite in order to be able to actually move the window when
													// dragged
		x = event.getSceneX();
		y = event.getSceneY();
	}
	/**
	 * Method used to close the window
	 * 
	 * @param event		Used to get the stage
	 */
	@FXML
	public void closeWindow(MouseEvent event) {
		((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
	}
	/**
	 * 
	 * Method used to minimize the window
	 * 
	 * @param event		Used to get the stage
	 */
	@FXML
	public void minimizeWindow(MouseEvent event) {
		((Stage) ((Node) event.getSource()).getScene().getWindow()).setIconified(true);
	}
	
	/**
	 * 
	 * Method called when a user tries to login.
	 * Based on the CNP provided it will search that client in the database
	 * and if it exist it'll allow him to log in
	 * 
	 */
	@FXML
	public void login() {
		ResultSet temp = Connector.findByID(cnp.getText(), "client", "clientID");
		MainController.setClientID(cnp.getText());
		if (temp != null) {
			content.getChildren().clear();
			content.getChildren().add(FXMLFactory.getFXML("clientSide"));
			
		} else {
			error.setVisible(true);
		}
	}

	/**
	 * 
	 * Method that is called when a user search for products, displaying all the products
	 * and the ability to add them to cart
	 * 
	 */
	@FXML
	public void searchProducts() {
		Thread background = new Thread(new Runnable() {
			
			@Override
			public void run() {
				productList.clear();
				Platform.runLater(() ->  products.clear());
				Platform.runLater(() ->  buttons.clear());
				currentBill.clear();

				sp.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
				vBox.setSpacing(20);
				vBoxButtons.setSpacing(12);
				products = vBox.getChildren();
				buttons = vBoxButtons.getChildren();

				ResultSet temp2 = Connector.searchObject(new Product("", "", "", "", ""));
				try {
					while (temp2.next()) {
						Product temporary = new Product(temp2.getString(1), temp2.getString(2), temp2.getString(3),
								temp2.getString(4), temp2.getString(5));
						Platform.runLater(() -> productList.add(temporary));
						Platform.runLater(() -> displayProducts(temporary));
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		background.start();
	}

	/**
	 * 
	 * Displaying the found products one after the other
	 * 
	 * @param temporary		A given product from the list of the found products currently in the database
	 */
	private void displayProducts(Product temporary) {

		HBox temp = new HBox();
		temp.setSpacing(70);

		temp.getChildren().add(new Label(temporary.getProductID()));
		temp.getChildren().add(new Label(temporary.getDescription()));
		temp.getChildren().add(new Label(temporary.getPrice()));
		temp.getChildren().add(new Label(temporary.getAmount()));
		temp.getChildren().add(new Label(temporary.getCategory()));

		products.add(temp);

		HBox temp1 = new HBox();
		temp1.setSpacing(10);

		Button minus = new Button("<");
		minus.setId("smallButtons");
		minus.setOnAction(e -> buttonPressedMinus(minus));
		Button plus = new Button(">");
		plus.setId("smallButtons");
		plus.setOnAction(e -> buttonPressedPlus(plus));
		Label qty = new Label("0");
		temp1.getChildren().addAll(minus, qty, plus);
		buttons.add(temp1);
	}
	/**
	 * 
	 * Method called when the button to decrease the current quantity for a product is pressed
	 * 
	 * @param input		Finding the product that corresponds to this button in order to decrease 1 from cart
	 */
	private void buttonPressedMinus(Button input) { // decrease the quantity for this order
		int i;
		for (i = 0; i < buttons.size(); i++) {
			HBox temp = (HBox) buttons.get(i);
			if (temp.getChildren().contains(input)) {
				Label current = (Label) temp.getChildren().get(1);
				int nr = Integer.valueOf(current.getText());
				if (nr > 0) {
					nr--;
					temp.getChildren().remove(1);
					temp.getChildren().add(1, new Label(String.valueOf(nr)));
				}
				break;
			}
		}
	}
	/**
	 * 
	 * Method called when the button to increase the current quantity for a product is pressed
	 * 
	 * @param input		Finding the product that corresponds to this button in order to increase 1 from cart
	 */
	private void buttonPressedPlus(Button input) { // increase the quantity for this order
		int i;
		for (i = 0; i < buttons.size(); i++) {
			HBox temp = (HBox) buttons.get(i);
			if (temp.getChildren().contains(input)) {
				Label current = (Label) temp.getChildren().get(1);
				int nr = Integer.valueOf(current.getText());
				if (nr < Integer.valueOf(productList.get(i).getAmount())) {
					nr++;
					temp.getChildren().remove(1);
					temp.getChildren().add(1, new Label(String.valueOf(nr)));
				}
				break;
			}
		}
	}
	/**
	 * 
	 * Method called when the order button is pressed.
	 * It will issue the invoice number by extracting information of the customer that ordered the item
	 * and getting all the products ordered and printing everything to a .pdf file
	 * After the .pdf file was written, the searchProducts() method is called again, resetting the cart.
	 */
	@FXML
	public void orderProducts() {
		int orderID = new Random().nextInt(222222222);
		
		
		java.util.Date dt = new java.util.Date();
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(dt);
		
		//here should be made a check, such that if no product was added no entry is added to the database
		//currently this is a flow !!
		Connector.insertObject(new Order(String.valueOf(orderID), currentTime, MainController.getClientID())); // create the order
		
		for (int i = 0; i < productList.size(); i++) {
			HBox temp = (HBox) buttons.get(i);
			int amnt = Integer.valueOf(((Label) temp.getChildren().get(1)).getText());
			if (amnt > 0) {
				String amount = String.valueOf(amnt);
				String price = productList.get(i).getPrice();
				String total = String.valueOf(Integer.valueOf(amount)*Integer.valueOf(price));
				currentBill.add(new Bill(productList.get(i).getDescription(), amount, price, total));
				int orderDetailsId = new Random().nextInt(222222222);
				Connector.insertObject(new OrderDetails(Integer.valueOf(orderDetailsId), Integer.valueOf(productList.get(i).getProductID()),
						Integer.valueOf(amount), Integer.valueOf(price), Integer.valueOf(orderID))); // create the order details
			}
		}
		Invoice.writeInvoice(currentBill, String.valueOf(orderID), currentTime);
		searchProducts();
	}
}
