package presentation.product;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.jfoenix.controls.JFXTextField;

import database.Search;
import database.Connector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import model.Product;
/**
 * Controller invoked when the search product button is pressed
 * 
 * @author Pirosca Calin Lucian
 *
 */
public class searchProduct {

	@FXML
	JFXTextField pidText, priceText, qtyText, descText;

	@FXML
	TableView<Product> table;
	private final static ObservableList<Product> dataList = FXCollections.observableArrayList();

	@FXML
	private TableColumn<Product, String> numeCol, prenumeCol, CNPCol, adresaCol;

	volatile ResultSet Products;
	/**
	 * 
	 * Method used to initialize the table.
	 * It will first get all the products from the database
	 * and through reflection it will compute the header of the table
	 * and add all the values inside the table.
	 * 
	 * Using text listeners the table will be updated in real time
	 * whenever a new character is added or deleted to any of the search
	 * fields
	 */
	public void initialize() {
		dataList.clear();
		Products = Connector.searchObject(new Product(pidText.getText(), priceText.getText(), qtyText.getText(), descText.getText(),"%"));

		/*numeCol.setCellValueFactory(new PropertyValueFactory<>("LastName")); // these values must match the fields in the DB
		prenumeCol.setCellValueFactory(new PropertyValueFactory<>("FirstName"));
		CNPCol.setCellValueFactory(new PropertyValueFactory<>("ProductID"));
		adresaCol.setCellValueFactory(new PropertyValueFactory<>("Address"));*/
		table.setItems(dataList);
		table.getColumns().clear();
		instantiate();
		//table.getColumns().addAll(numeCol,prenumeCol, CNPCol, adresaCol);

		pidText.textProperty().addListener((observable, oldValue, newValue) -> {
			synchronized (Products) {
				Products = Connector.searchObject(new Product(pidText.getText(), priceText.getText(), qtyText.getText(), descText.getText(),"%"));
				try {
					if (Products != null) {
						dataList.clear();
						Products.beforeFirst();
						instantiate();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		priceText.textProperty().addListener((observable, oldValue, newValue) -> {
			synchronized (Products) {
				Products = Connector.searchObject(new Product(pidText.getText(), priceText.getText(), qtyText.getText(), descText.getText(),"%"));
				try {
					if (Products != null) {
						dataList.clear();
						Products.beforeFirst();
						instantiate();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		qtyText.textProperty().addListener((observable, oldValue, newValue) -> {
			synchronized (Products) {
				Products = Connector.searchObject(new Product(pidText.getText(), priceText.getText(), qtyText.getText(), descText.getText(),"%"));
				try {
					if (Products != null) {
						dataList.clear();
						Products.beforeFirst();
						instantiate();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		descText.textProperty().addListener((observable, oldValue, newValue) -> {
			synchronized (Products) {
				Products = Connector.searchObject(new Product(pidText.getText(), priceText.getText(), qtyText.getText(), descText.getText(),"%"));
				try {
					if (Products != null) {
						dataList.clear();
						Products.beforeFirst();
						instantiate();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}	
	/**
	 * 
	 * Method used to populate the table through reflection and 
	 * compute its header based on the given search values
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void instantiate() {
		table.getColumns().clear();
		Search getValues = new Search();
		List<Product> Producti = new ArrayList<Product>();
		Producti = getValues.populate(Products, table);
		for(Product temp : Producti) {
			dataList.add(temp);
		}
	}
}
