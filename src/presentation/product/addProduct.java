package presentation.product;

import com.jfoenix.controls.JFXTextField;

import business.validator.Input;
import database.Connector;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import model.Product;
/**
 * Controller invoked when the add product button is pressed
 * 
 * @author Pirosca Calin Lucian
 *
 */
public class addProduct {

	@FXML
	JFXTextField pid, price, amnt, desc;

	@FXML
	Label error, success;
	
	@FXML
	ChoiceBox<String> category;

	ObservableList<String> categoryList = FXCollections.observableArrayList("Food",
			"Clothes", "Tools");
	
	/**
	 * 
	 * Initializes the list for the category of the products with the
	 * default values and sets the initial one was nothing.
	 * 
	 */
	@FXML
	private void initialize() {
		category.setItems(categoryList);
		category.setValue("");
	}
	/**
	 * 
	 * Method called when the add button is pressed
	 * It will check each input, stopping at the first invalid input and
	 * displaying what is wrong. If all the input fields are valid, then
	 * it will try to add the product.
	 * Based on the insertion,whether is was successful or not,
	 * an appropriate label will be displayed.
	 * 
	 */
	@FXML
	public void add() {
		Thread back = new Thread(new Runnable() { // using back thread to ensure smoothness of the application
			
			@Override
			public void run() {
				if (Input.validation(pid) == true) {
					if (Input.validation(price) == true) {
						if (Input.validation(amnt) == true) {
							if (desc.getText().length() > 0 && category.getValue().length() > 0) {
								boolean temp = Connector.insertObject(new Product(pid.getText(),price.getText(),amnt.getText(), desc.getText(),getProductCategory(category.getValue())));
								if (temp == false) {
									success.setVisible(false);
									error.setVisible(true);
								} else {
									error.setVisible(false);
									success.setVisible(true);
									pid.setText("");
									price.setText("");
									desc.setText("");
									amnt.setText("");
									pid.setPromptText("");
									price.setPromptText("");
									desc.setPromptText("");
									amnt.setPromptText("");
								}
							}else {
								Platform.runLater(() ->desc.setPromptText("Cannot be empty"));
							}
						} else {
							success.setVisible(false);
							error.setVisible(false);
						}
					} else {
						success.setVisible(false);
						error.setVisible(false);
					}
				} else {
					success.setVisible(false);
					error.setVisible(false);
				}
			}
		});
		back.start();
	}
	
	private String getProductCategory(String category) {
		switch(category) {
		case "Food":
			return "Food";
		case "Clothes":
			return "Clothes";
		case "Tools":
			return "Tools";
		default:
			return "Others";
		}
	}
}
