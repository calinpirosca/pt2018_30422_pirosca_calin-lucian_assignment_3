package presentation.product;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import business.validator.Input;
import database.Connector;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import model.Product;

/**
 * Controller invoked when the edit product button is pressed
 * 
 * @author Pirosca Calin Lucian
 *
 */

public class editProduct {

	private String pid1; // use to save the initial values before update

	@FXML
	JFXTextField cnp, pidText, priceText, amntText, descText,pidFind;

	@FXML
	Label error, pid, price, amnt, desc, info;

	@FXML
	JFXButton updateButton;
	
	String category;
	/**
	 * 
	 * Method used to hide/display the textfields and labels
	 * It will hide them at first, and display them when the searched
	 * product is found. After updating the product they will be hidden again
	 * 
	 * @param check		Deciding whether to hide them or not
	 */
	private void display(boolean check) {
		pid.setVisible(check);
		price.setVisible(check);
		amnt.setVisible(check);
		desc.setVisible(check);
		pidText.setVisible(check);
		priceText.setVisible(check);
		amntText.setVisible(check);
		descText.setVisible(check);
		info.setVisible(check);
	}
	/**
	 * 
	 * Method used to find the product based on the given input
	 * If the input is valid it will search for the product with the given
	 * ID in the database and return the corresponding values so that they
	 * can be modified and then updated.
	 * If the initial input textfield is not valid a label will be displayed.
	 * If the product cannot be updated or was not found a label will be displayed
	 * Upon successful update, another label will be displayed
	 * 
	 */
	@FXML
	public void find() {
		error.setVisible(false);
		Thread back = new Thread(new Runnable() { // using a back end thread to ensure smoothness of the application,
													// otherwise there would be a slight lag
			// when pressing the "find" button
			@Override
			public void run() {
				if (!pidFind.getText().matches("[0-9]+")) {
					Platform.runLater(() -> {
						error.setVisible(true);
						pidFind.setText("");
						pidFind.setPromptText("Only digits !");
					});
				} else {
					updateButton.setVisible(false);
					ResultSet data = Connector.findByID(pidFind.getText(), "product", "ProductID");
					if (data != null) {
						Platform.runLater(() -> {
							try {
								display(true);
								info.setTextFill(Color.BLACK);
								info.setText("Modify the current values : ");
								pidText.setText(data.getString(1));
								priceText.setText(data.getString(2));
								amntText.setText(data.getString(3));
								descText.setText(data.getString(4));
								category = data.getString(5);
								updateButton.setVisible(true);
							} catch (SQLException e) {
								System.out.println(e.getMessage());
							}
						});

						pid1 = pidFind.getText(); // saving the cnp so that the displayed one will be deleted in case if
												// the text field of cnp is change meanwhile
						// without updating the displayed one
					} else {
						Platform.runLater(() -> {
							display(false);
							info.setText("No entry found!");
							info.setTextFill(Color.web("#da0404"));
							info.setVisible(true);
						});
					}
				}
			}
		});
		back.start();
	}
	/**
	 * 
	 * Method available to be called only after the find() method was called
	 * After having displayed the product that is going to be updated
	 * and insert the modified values
	 * the update button will become available and the user will be able
	 * to update that particular product.
	 * Whether the product can or cannot be update a label will be displayed
	 * 
	 */
	@FXML
	public void update() {
		boolean temp;
		if (Input.validation(pidText) && Input.validation(priceText)
				&& Input.validation(amntText) && descText.getText().length() > 0) {
			temp = Connector.updateObject(new Product(pid1, priceText.getText(), amntText.getText(), descText.getText(),category));
			display(false);
			updateButton.setVisible(false);
			if (temp == false) {
				info.setText("Couldn't update !");
				info.setTextFill(Color.web("#da0404"));
				info.setVisible(true);
			} else {
				info.setText("Product succesfully updated!");
				info.setTextFill(Color.web("#049e19"));
				info.setVisible(true);
			}
		}else {
			info.setText("Invalid input!");
			info.setTextFill(Color.web("#da0404"));
			info.setVisible(true);
		}
	}
}
