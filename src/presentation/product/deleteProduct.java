package presentation.product;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import database.Connector;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

/**
 * Controller invoked when the delete product button is pressed
 * 
 * @author Pirosca Calin Lucian
 *
 */

public class deleteProduct {

	private String PID = "";

	@FXML
	JFXTextField pid;

	@FXML
	Label error, info;

	@FXML
	JFXButton deleteButton;

	/**
	 * 
	 * Method used to find the product based on the given input
	 * If the input is valid it will search for the product with the given
	 * ID in the database and return the corresponding values so that they
	 * can be checked and then deleted
	 * If the initial input textfield is not valid a label will be displayed.
	 * Upon successful delete, another label will be displayed
	 * 
	 * @see delete()
	 * 
	 */
	@FXML
	public void find() {
		error.setVisible(false);
		Thread back = new Thread(new Runnable() { // using a back end thread to ensure smoothness of the application, otherwise there would be a slight lag
			//when pressing the "find" button

			@Override
			public void run() {
				if (!pid.getText().matches("[0-9]+")) {
					Platform.runLater(() -> {
						error.setVisible(true);
						pid.setText("");
						pid.setPromptText("Only digits !");
					});
				} else {
					deleteButton.setVisible(false);
					ResultSet data = Connector.findByID(pid.getText(), "product", "ProductID");
					if (data != null) {
						Platform.runLater(() -> {
							try {
								info.setText("Price = " + data.getString(2) + "\nAmount = " + data.getString(3)
										+ "\nProductID = " + data.getString(1) + "\nDescription = " + data.getString(4));
								info.setTextFill(Color.BLACK);
								info.setVisible(true);
								deleteButton.setVisible(true);
							} catch (SQLException e) {
								System.out.println(e.getMessage());
							}
						});

						PID = pid.getText(); // saving the pid so that the displayed one will be deleted in case if
												// the text field of pid is change meanwhile
						// without updating the displayed one
					} else {
						Platform.runLater(() -> {
							info.setText("No entry found!");
							info.setTextFill(Color.web("#da0404"));
							info.setVisible(true);
						});
					}
				}
			}
		});
		back.start();
	}

	/**
	 * 
	 * Method available to be called only after the find() method was called
	 * After having displayed the product that is going to be deleted
	 * the delete button will become available and the user will be able
	 * to delete that particular product.
	 * Whether the product can or cannot be deleted a label will be displayed
	 * 
	 */
	@FXML
	public void delete() {
		if (Connector.deleteByID(PID, "product", "ProductID")) { // using PID instead of PID.getText I will make sure to delete only the last
											// one that was displayed !
			info.setText("Successfully deleted !");
			info.setTextFill(Color.web("#049e19"));
			deleteButton.setVisible(false);
		} else {
			info.setText("Unknown error\nCouldn't delete!");
			info.setTextFill(Color.web("#da0404"));
			info.setVisible(true);
		}
	}

}
