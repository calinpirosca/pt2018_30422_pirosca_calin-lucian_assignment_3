package presentation;

import java.util.ArrayList;
import java.util.List;

import business.FXMLFactory;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * 
 * Controller used for the Client Panel.
 * It allows adding, modifying, deleting or displaying all the Clients currently
 * stored in the database
 * 
 * @author Pirosca Calin Lucian
 *
 */

public class ProductController {

	MainController main;
	double x, y;

	List<AnchorPane> panes = new ArrayList<AnchorPane>();
	List<Label> labels = new ArrayList<Label>();
	List<ImageView> images = new ArrayList<ImageView>();

	@FXML
	AnchorPane mainWindow,splitPane, addPane, editPane, deletePane, searchPane, content;

	@FXML
	Label addLabel, editLabel, deleteLabel, searchLabel;

	@FXML
	ImageView addImage, editImage, deleteImage, searchImage, minimize;

	@FXML
	HBox titleBar;
	/**
	 * 
	 * initializes the GUI
	 * 
	 */
	public void initialize() {
		panes.add(addPane);
		panes.add(editPane);
		panes.add(deletePane);
		panes.add(searchPane);
		labels.add(addLabel);
		labels.add(editLabel);
		labels.add(deleteLabel);
		labels.add(searchLabel);
		images.add(addImage);
		images.add(editImage);
		images.add(deleteImage);
		images.add(searchImage);
	}

	public void setMain(MainController mainController) {
		main = mainController;
	}
	/**
	 * 
	 * Method used to set the opacity back to 1 after the window was moved
	 * 
	 * @param event		Used to get the stage
	 */
	@FXML
	public void windowMoved(MouseEvent event) {
		((Stage) ((Node) event.getSource()).getScene().getWindow()).setOpacity(1);
	}
	/**
	 * Method used to move the window and set the opacity to 0.7 while moving it
	 * 
	 * @param event		Used to get the stage
	 */
	@FXML
	public void moveWindow(MouseEvent event) {
		Stage temp = (Stage) ((Node) event.getSource()).getScene().getWindow();
		temp.setX(event.getScreenX() - x);
		temp.setY(event.getScreenY() - y);
		temp.setOpacity(0.7);
	}
	/**
	 * 
	 * Prerequisite method in order to be able to actually move the window when dragged
	 * Storing the initial position
	 * 
	 * @param event		Used to get the current position of the window
	 */
	@FXML
	public void moveWindowPre(MouseEvent event) { // prerequisite in order to be able to actually move the window when
													// dragged
		x = event.getSceneX();
		y = event.getSceneY();
	}
	/**
	 * Method used to close the window
	 * 
	 * @param event		Used to get the stage
	 */
	@FXML
	public void closeWindow(MouseEvent event) {
		((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
	}
	/**
	 * 
	 * Method used to minimize the window
	 * 
	 * @param event		Used to get the stage
	 */
	@FXML
	public void minimizeWindow(MouseEvent event) {
		((Stage) ((Node) event.getSource()).getScene().getWindow()).setIconified(true);
	}
	/**
	 * Method used to go back to the main window
	 * 
	 * @param event		Used to get the stage
	 */
	@FXML
	public void goBack(MouseEvent event) {
		main.display();
		closeWindow(event);
	}
	/**
	 * 
	 * Used to update the window based on the buttons pressed
	 * 
	 * @param type  Deciding which FXML file to be loaded based on a given String.
	 * 
	 * @see addProduct()
	 * @see editProduct()
	 * @see deleteProduct()
	 * @see searchProduct()
	 */
	public void loadFXML(String type) {
		content.getChildren().clear(); // used to remove the previously loaded fxml files
		content.getChildren().add(FXMLFactory.getFXML(type));
		if (!content.isVisible()) {
			Thread temp = new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						Thread.sleep(400);
						content.setVisible(true);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
			temp.start();
		}
	}
	/**
	 * 
	 * Method called when the add product button is pressed.
	 * It loads an FXML that contains elements that allows the user
	 * to add a product
	 * 
	 */
	@FXML
	public void addProduct() {
		if (!content.isVisible()) {
			Animation.animateSplitBar(splitPane, panes, labels, images);// resizes all the "anchor pane buttons",
																		// labels, imageviews
		}
		loadFXML("addproduct");
	}
	/**
	 * 
	 * Method called when the edit product button is pressed.
	 * It loads an FXML that contains elements that allows the user
	 * to edit a product
	 * 
	 */
	@FXML
	public void editProduct() {
		if (!content.isVisible()) {
			Animation.animateSplitBar(splitPane, panes, labels, images);// resizes all the "anchor pane buttons",
																		// labels, imageviews
		}
		loadFXML("editproduct");
	}
	/**
	 * 
	 * Method called when the delete product button is pressed.
	 * It loads an FXML that contains elements that allows the user
	 * to delete a product
	 * 
	 */
	@FXML
	public void deleteProduct() {
		if (!content.isVisible()) {
			Animation.animateSplitBar(splitPane, panes, labels, images);// resizes all the "anchor pane buttons",
																		// labels, imageviews
		}
		loadFXML("deleteproduct");
	}
	/**
	 * 
	 * Method called when the search product button is pressed.
	 * It loads an FXML that contains elements that allows the user
	 * to search a product
	 * 
	 */
	@FXML
	public void searchProduct() {
		if (!content.isVisible()) {
			Animation.animateSplitBar(splitPane, panes, labels, images);// resizes all the "anchor pane buttons",
																		// labels, imageviews
		}
		loadFXML("searchproduct");
	}
}
