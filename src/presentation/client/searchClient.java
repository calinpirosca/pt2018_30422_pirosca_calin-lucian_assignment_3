package presentation.client;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.jfoenix.controls.JFXTextField;

import database.Search;
import database.Connector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import model.Client;
/**
 * Controller invoked when the search client button is pressed
 * 
 * @author Pirosca Calin Lucian
 *
 */
public class searchClient {

	@FXML
	JFXTextField numeText, prenumeText, cnpText, adresaText;

	@FXML
	TableView<Client> table;
	private final static ObservableList<Client> dataList = FXCollections.observableArrayList();

	@FXML
	private TableColumn<Client, String> numeCol, prenumeCol, CNPCol, adresaCol;

	volatile ResultSet clients;
	
	/**
	 * 
	 * Method used to initialize the table.
	 * It will first get all the clients from the database
	 * and through reflection it will compute the header of the table
	 * and add all the values inside the table.
	 * 
	 * Using text listeners the table will be updated in real time
	 * whenever a new character is added or deleted to any of the search
	 * fields
	 * 
	 */
	public void initialize() {
		dataList.clear();
		clients = Connector.searchObject(new Client(numeText.getText(), prenumeText.getText(), cnpText.getText(), adresaText.getText()));

		/*numeCol.setCellValueFactory(new PropertyValueFactory<>("LastName")); // these values must match the fields in the DB
		prenumeCol.setCellValueFactory(new PropertyValueFactory<>("FirstName"));
		CNPCol.setCellValueFactory(new PropertyValueFactory<>("ClientID"));
		adresaCol.setCellValueFactory(new PropertyValueFactory<>("Address"));*/
		table.setItems(dataList);
		table.getColumns().clear();
		instantiate();
		//table.getColumns().addAll(numeCol,prenumeCol, CNPCol, adresaCol);

		numeText.textProperty().addListener((observable, oldValue, newValue) -> {
			synchronized (clients) {
				clients = Connector.searchObject(new Client(numeText.getText(), prenumeText.getText(), cnpText.getText(), adresaText.getText()));
				try {
					if (clients != null) {
						dataList.clear();
						clients.beforeFirst();
						instantiate();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		prenumeText.textProperty().addListener((observable, oldValue, newValue) -> {
			synchronized (clients) {
				clients = Connector.searchObject(new Client(numeText.getText(), prenumeText.getText(), cnpText.getText(), adresaText.getText()));
				try {
					if (clients != null) {
						dataList.clear();
						clients.beforeFirst();
						instantiate();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		cnpText.textProperty().addListener((observable, oldValue, newValue) -> {
			synchronized (clients) {
				clients = Connector.searchObject(new Client(numeText.getText(), prenumeText.getText(), cnpText.getText(), adresaText.getText()));
				try {
					if (clients != null) {
						dataList.clear();
						clients.beforeFirst();
						instantiate();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		adresaText.textProperty().addListener((observable, oldValue, newValue) -> {
			synchronized (clients) {
				clients = Connector.searchObject(new Client(numeText.getText(), prenumeText.getText(), cnpText.getText(), adresaText.getText()));
				try {
					if (clients != null) {
						dataList.clear();
						clients.beforeFirst();
						instantiate();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		//startThreads();
	}
	/**
	 * 
	 * Method used to populate the table through reflection and 
	 * compute its header based on the given search values
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void instantiate() {
		table.getColumns().clear();
		Search getValues = new Search();
		List<Client> clienti = new ArrayList<Client>();
		clienti = getValues.populate(clients, table);
		for(Client temp : clienti) {
			dataList.add(temp);
		}
	}
	
	/*public void startThreads() { // old way of doing things, before reflection
		Thread loadTable = new Thread(new Runnable() {
			@Override
			synchronized public void run() {
				while (stop) {
					synchronized (clients) {
						try {
							if (clients != null) {
								while (clients.next()) {
									addClient(new Client(clients.getString(3), clients.getString(4),
											clients.getString(1), clients.getString(2)));
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
							System.out.println(e.getMessage());
						}
					}
				}
			}
		});
		loadTable.start();
	}*/
}
