package presentation.client;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import database.Connector;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
/**
 * Controller invoked when the delete client button is pressed
 * 
 * @author Pirosca Calin Lucian
 *
 */
public class deleteClient {

	private String CNP = "";

	@FXML
	JFXTextField cnp;

	@FXML
	Label error, info;

	@FXML
	JFXButton deleteButton;
	/**
	 * 
	 * Method used to find the client based on the given input
	 * If the input is valid it will search for the client with the given
	 * ID in the database and return the corresponding values so that they
	 * can be checked and then deleted
	 * If the initial input textfield is not valid a label will be displayed.
	 * Upon successful delete, another label will be displayed
	 * 
	 * @see delete()
	 * 
	 */
	@FXML
	public void find() {
		error.setVisible(false);
		Thread back = new Thread(new Runnable() { // using a back end thread to ensure smoothness of the application, otherwise there would be a slight lag
			//when pressing the "find" button

			@Override
			public void run() {
				if (cnp.getText().length() != 13 || !cnp.getText().matches("[0-9]+")) {
					Platform.runLater(() -> {
						error.setVisible(true);
						cnp.setText("");
						cnp.setPromptText("Only last 13 digits of CNP");
					});
				} else {
					deleteButton.setVisible(false);
					ResultSet data = Connector.findByID(cnp.getText(), "client", "ClientID");
					if (data != null) {
						Platform.runLater(() -> {
							try {
								info.setText("First name = " + data.getString(3) + "\nLastName = " + data.getString(4)
										+ "\nCNP = " + data.getString(1) + "\nAddress = " + data.getString(2));
								info.setTextFill(Color.BLACK);
								info.setVisible(true);
								deleteButton.setVisible(true);
							} catch (SQLException e) {
								System.out.println(e.getMessage());
							}
						});

						CNP = cnp.getText(); // saving the cnp so that the displayed one will be deleted in case if
												// the text field of cnp is change meanwhile
						// without updating the displayed one
					} else {
						Platform.runLater(() -> {
							info.setText("No entry found!");
							info.setTextFill(Color.web("#da0404"));
							info.setVisible(true);
						});
					}
				}
			}
		});
		back.start();
	}
	/**
	 * 
	 * Method available to be called only after the find() method was called
	 * After having displayed the client that is going to be deleted
	 * the delete button will become available and the user will be able
	 * to delete that particular client.
	 * Whether the client can or cannot be deleted a label will be displayed
	 * 
	 */
	@FXML
	public void delete() {
		if (Connector.deleteByID(CNP, "client", "ClientID")) { // using CNP instead of cnp.getText I will make sure to delete only the last
											// one that was displayed !
			info.setText("Successfully deleted !");
			info.setTextFill(Color.web("#049e19"));
			deleteButton.setVisible(false);
		} else {
			info.setText("Unknown error\nCouldn't delete!");
			info.setTextFill(Color.web("#da0404"));
			info.setVisible(true);
		}
	}

}
