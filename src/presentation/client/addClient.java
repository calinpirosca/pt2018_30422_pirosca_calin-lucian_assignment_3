package presentation.client;

import com.jfoenix.controls.JFXTextField;

import business.validator.Input;
import database.Connector;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import model.Client;

/**
 * Controller invoked when the add client button is pressed
 * 
 * @author Pirosca Calin Lucian
 *
 */

public class addClient {

	@FXML
	JFXTextField lname, fname, cnp, address;

	@FXML
	Label error, success;

	/**
	 * 
	 * Method called when the add button is pressed
	 * It will check each input, stopping at the first invalid input and
	 * displaying what is wrong. If all the input fields are valid, then
	 * it will try to add the client.
	 * Based on the insertion,whether is was successful or not,
	 * an appropriate label will be displayed.
	 * 
	 */
	@FXML
	public void add() {
		Thread back = new Thread(new Runnable() { // using back thread to ensure smoothness of the application
			
			@Override
			public void run() {
				if (Input.validate(fname, true) == true) {
					if (Input.validate(lname, true) == true) {
						if (Input.validate(cnp, false) == true) {
							if (address.getText().length() > 0) {
								boolean temp = Connector.insertObject(new Client(lname.getText(),fname.getText(),cnp.getText(), address.getText()));
								if (temp == false) {
									success.setVisible(false);
									error.setVisible(true);
								} else {
									error.setVisible(false);
									success.setVisible(true);
									fname.setText("");
									lname.setText("");
									cnp.setText("");
									address.setText("");
									fname.setPromptText("");
									lname.setPromptText("");
									cnp.setPromptText("");
									address.setPromptText("");
								}
							}else {
								Platform.runLater(() ->address.setPromptText("Cannot be empty"));
							}
						} else {
							success.setVisible(false);
							error.setVisible(false);
						}
					} else {
						success.setVisible(false);
						error.setVisible(false);
					}
				} else {
					success.setVisible(false);
					error.setVisible(false);
				}
			}
		});
		back.start();
	}
}
