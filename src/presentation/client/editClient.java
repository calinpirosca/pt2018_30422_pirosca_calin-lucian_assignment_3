package presentation.client;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import business.validator.Input;
import database.Connector;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import model.Client;
/**
 * Controller invoked when the edit client button is pressed
 * 
 * @author Pirosca Calin Lucian
 *
 */
public class editClient {

	private String CNP; // use to save the initial values before update

	@FXML
	JFXTextField cnp, fnametext, lnametext, cnp1text, addresstext;

	@FXML
	Label error, fname, lname, cnp1, address, info;

	@FXML
	JFXButton updateButton;
	/**
	 * 
	 * Method used to hide/display the textfields and labels
	 * It will hide them at first, and display them when the searched
	 * client is found. After updating the client they will be hidden again
	 * 
	 * @param check		Deciding whether to hide them or not
	 */
	private void display(boolean check) {
		fname.setVisible(check);
		lname.setVisible(check);
		cnp1.setVisible(check);
		address.setVisible(check);
		addresstext.setVisible(check);
		cnp1text.setVisible(check);
		lnametext.setVisible(check);
		fnametext.setVisible(check);
		info.setVisible(check);
	}
	/**
	 * 
	 * Method used to find the product based on the given input
	 * If the input is valid it will search for the product with the given
	 * ID in the database and return the corresponding values so that they
	 * can be modified and then updated.
	 * If the initial input textfield is not valid a label will be displayed.
	 * If the product cannot be updated or was not found a label will be displayed
	 * Upon successful update, another label will be displayed
	 * 
	 */
	@FXML
	public void find() {
		error.setVisible(false);
		Thread back = new Thread(new Runnable() { // using a back end thread to ensure smoothness of the application,
													// otherwise there would be a slight lag
			// when pressing the "find" button

			@Override
			public void run() {
				if (cnp.getText().length() != 13 || !cnp.getText().matches("[0-9]+")) {
					Platform.runLater(() -> {
						error.setVisible(true);
						cnp.setText("");
						cnp.setPromptText("Only last 13 digits of CNP");
					});
				} else {
					updateButton.setVisible(false);
					ResultSet data = Connector.findByID(cnp.getText(), "client", "ClientID");
					if (data != null) {
						Platform.runLater(() -> {
							try {
								display(true);
								info.setTextFill(Color.BLACK);
								info.setText("Modify the current values : ");
								fnametext.setText(data.getString(3));
								lnametext.setText(data.getString(4));
								cnp1text.setText(data.getString(1));
								addresstext.setText(data.getString(2));
								updateButton.setVisible(true);
							} catch (SQLException e) {
								System.out.println(e.getMessage());
							}
						});

						CNP = cnp.getText(); // saving the cnp so that the displayed one will be deleted in case if
												// the text field of cnp is change meanwhile
						// without updating the displayed one
					} else {
						Platform.runLater(() -> {
							display(false);
							info.setText("No entry found!");
							info.setTextFill(Color.web("#da0404"));
							info.setVisible(true);
						});
					}
				}
			}
		});
		back.start();
	}
	/**
	 * 
	 * Method available to be called only after the find() method was called
	 * After having displayed the client that is going to be updated
	 * and insert the modified values
	 * the update button will become available and the user will be able
	 * to update that particular client.
	 * Whether the client can or cannot be update a label will be displayed
	 * 
	 */
	@FXML
	public void update() {
		boolean temp;
		if (Input.validate(lnametext, true) && Input.validate(fnametext, true)
				&& Input.validate(cnp1text, false) && addresstext.getText().length() > 0) {
			temp = Connector.updateObject(new Client(lnametext.getText(),fnametext.getText(),CNP, addresstext.getText()));
			display(false);
			updateButton.setVisible(false);
			if (temp == false) {
				info.setText("Couldn't update !");
				info.setTextFill(Color.web("#da0404"));
				info.setVisible(true);
			} else {
				info.setText("Client succesfully updated!");
				info.setTextFill(Color.web("#049e19"));
				info.setVisible(true);
			}
		}else {
			info.setText("Invalid input!");
			info.setTextFill(Color.web("#da0404"));
			info.setVisible(true);
		}
	}

}
