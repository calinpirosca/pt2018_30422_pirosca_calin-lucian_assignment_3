package presentation;

import java.util.List;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.animation.Animation.Status;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.util.Duration;

/**
 * Class used to move and resize ( basically update the GUI ) when a button is pressed
 * inside the Client panel or inside the Product panel.
 * This class is called and is visible the first time when a button is pressed inside any of the
 * panels.
 * 
 * @author Pirosca Calin Lucian
 *
 */

public class Animation {

	private static PathTransition transition = new PathTransition();
	
	/**
	 * Method used to update the GUI, that is move some AnchorPanes and resize the buttons inside the 
	 * client panel or product panel
	 * 
	 * @param pane		The AnchorPane where the new information is displayed once a button is pressed
	 * @param panes		The list consisting of buttons that need to be moved and resized
	 * @param labels	The labels corresponding to the button that need to be reshaped
	 * @param images	The images corresponding to the button that need to be resized
	 * 
	 * @see animatePane(AnchorPane, Label,ImageView)
	 */
	public static void animateSplitBar(AnchorPane pane, List<AnchorPane> panes,List<Label> labels,List<ImageView> images) {// used to animate the splitbar
		if (transition.getStatus() != Status.RUNNING) {
			pane.setVisible(true);

			Path path = new Path();
			MoveTo moveTo = new MoveTo();
			moveTo.setX(0.0);
			moveTo.setY(240.0);

			LineTo lineTo = new LineTo(-200, 240);
			path.getElements().add(moveTo);
			path.getElements().add(lineTo);

			transition.setNode(pane);
			transition.setDuration(Duration.millis(400));
			transition.setPath(path);
			transition.setCycleCount(1);
			transition.play();
			for(int i=0;i<panes.size();i++) {
				animatePane(panes.get(i), labels.get(i), images.get(i));
			}
		}
	}
	/**
	 * 
	 * Method used to animate each button individually
	 * 
	 * 
	 * @param pane		The pane that encapsulates the image and label, becoming a button
	 * @param label		The label corresponding to the button
	 * @param image		The image associated with the button
	 * 
	 * @see animateSplitBar(AnchorPane, List<AnchorPane>,List<Label>,List<ImageView>)
	 * 
	 */
	public static void animatePane(AnchorPane pane, Label label, ImageView image) { // used to resize the button pane
		Timeline timeline = new Timeline();
		
		KeyValue labelValue = new KeyValue(label.prefWidthProperty(), 100); // used for label truncation
		KeyFrame labeltrunc = new KeyFrame(Duration.millis(0), labelValue);
		
		KeyValue labelValue1 = new KeyValue(label.prefWidthProperty(), 0);
		KeyFrame labeltrunc1 = new KeyFrame(Duration.millis(300), labelValue1); // end use of label truncation
		
		
		KeyValue imageMoveValue = new KeyValue(image.layoutXProperty(), 25); // used for moving imageview
		KeyFrame imageMove = new KeyFrame(Duration.millis(0), imageMoveValue);
		
		KeyValue imageMoveValue1 = new KeyValue(image.layoutXProperty(), 14);
		KeyFrame imageMove1 = new KeyFrame(Duration.millis(300), imageMoveValue1); // end used for moving imageview
		
		
		KeyValue labelValue2 = new KeyValue(label.layoutXProperty(), 80); // used for moving label
		KeyFrame labelmove = new KeyFrame(Duration.millis(0), labelValue2);
		
		KeyValue labelValue3 = new KeyValue(label.layoutXProperty(), 10);
		KeyFrame labelmove1 = new KeyFrame(Duration.millis(600), labelValue3); // end use of moving label
		//moving label otherwise I cannot properly resize the pane width
		
		
		KeyValue paneValue = new KeyValue(pane.prefWidthProperty(), 200); // used for pane resize width
		KeyFrame paneResize = new KeyFrame(Duration.millis(0), paneValue);
		
		KeyValue paneValue1 = new KeyValue(pane.prefWidthProperty(), 65);
		KeyFrame paneResize1 = new KeyFrame(Duration.millis(400), paneValue1); // end used for pane resize width
		
		
		KeyValue paneValue2 = new KeyValue(pane.layoutXProperty(), 85); // used for pane move
		KeyFrame paneMove = new KeyFrame(Duration.millis(0), paneValue2);
		
		KeyValue paneValue3 = new KeyValue(pane.layoutXProperty(), 40);
		KeyFrame paneMove1 = new KeyFrame(Duration.millis(400), paneValue3); // end used for pane move

		timeline.getKeyFrames().addAll(labeltrunc, labeltrunc1,imageMove, imageMove1, labelmove, labelmove1,paneResize,paneResize1,paneMove,paneMove1);
		timeline.play();
	}
}
