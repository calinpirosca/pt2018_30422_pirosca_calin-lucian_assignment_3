package presentation;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import start.Main;

/**
 * 
 * This controller corresponds to the main window.
 * 
 * @author Pirosca Calin Lucian
 *
 */

public class MainController {

	private double x, y;

	private ClientController client;
	private ProductController product;
	
	@FXML
	AnchorPane main;

	@FXML
	public void initialize() {
	}

	/**
	 * 
	 * Method called to re-open the main window if the "back" button is pressed from within 
	 * the client panel or product panel
	 * 
	 */
	public void display() {
		Stage temp = (Stage) main.getScene().getWindow();
		temp.show();
	}
	/**
	 * 
	 * Method used to set the opacity back to 1 after the window was moved
	 * 
	 * @param event		Used to get the stage
	 */
	@FXML
	public void windowMoved(MouseEvent event) {
		((Stage) ((Node) event.getSource()).getScene().getWindow()).setOpacity(1);
	}
	/**
	 * Method used to move the window and set the opacity to 0.7 while moving it
	 * 
	 * @param event		Used to get the stage
	 */
	@FXML
	public void moveWindow(MouseEvent event) {
		Stage temp = (Stage) ((Node) event.getSource()).getScene().getWindow();
		temp.setX(event.getScreenX() - x);
		temp.setY(event.getScreenY() - y);
		temp.setOpacity(0.7);
	}
	/**
	 * 
	 * Prerequisite method in order to be able to actually move the window when dragged
	 * Storing the initial position
	 * 
	 * @param event		Used to get the current position of the window
	 */
	@FXML
	public void moveWindowPre(MouseEvent event) { // prerequisite in order to be able to actually move the window when
													// dragged
		x = event.getSceneX();
		y = event.getSceneY();
	}

	/**
	 * Method used to close the window
	 * 
	 * @param event		Used to get the stage
	 */
	@FXML
	public void closeWindow(MouseEvent event) {
		((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
	}
	/**
	 * 
	 * Method used to minimize the window
	 * 
	 * @param event		Used to get the stage
	 */
	@FXML
	public void minimizeWindow(MouseEvent event) {
		((Stage) ((Node) event.getSource()).getScene().getWindow()).setIconified(true);
	}
	
	/**
	 * 
	 * JUST FOR TESTING! CLIENT SIDE PANEL OPENING !
	 * 
	 * Method called when the ClientSide button is pressed. It will load the corresponding
	 * fxml file and close to main window and open the CLIENTSIDE panel window
	 * 
	 * @param event		Used to get the current stage and close it
	 */
	@FXML
	public void openClient(MouseEvent event){ // this is used to switch to the client side of the application
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/fxml/ClientSide.fxml"));
			Parent root = (Parent) loader.load();
			Stage clientSide = new Stage();
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/view/css/application.css").toExternalForm());
			clientSide.initStyle(StageStyle.UNDECORATED);
			clientSide.setResizable(false);
			clientSide.setScene(scene);
			Main.setIcon(clientSide);
			clientSide.show();
			closeWindow(event);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * Method called when the client panel button is pressed. It will load the corresponding
	 * fxml file and close to main window and open the client panel window
	 * 
	 * @param event		Used to get the current stage and close it
	 */
	@FXML
	public void openClients(MouseEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/fxml/Client.fxml"));
			Parent root = (Parent) loader.load();
			client = loader.getController();
			client.setMain(this);
			Stage clients = new Stage();
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/view/css/application.css").toExternalForm());
			clients.initStyle(StageStyle.UNDECORATED);
			clients.setResizable(false);
			clients.setScene(scene);
			Main.setIcon(clients);
			clients.show();
			closeWindow(event);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * Method called when the product panel button is pressed. It will load the corresponding
	 * fxml file and close to main window and open the product panel window
	 * 
	 * @param event		Used to get the current stage and close it
	 */
	@FXML
	public void openProducts(MouseEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/fxml/Product.fxml"));
			Parent root = (Parent) loader.load();
			product = loader.getController();
			product.setMain(this);
			Stage products = new Stage();
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/view/css/application.css").toExternalForm());
			products.initStyle(StageStyle.UNDECORATED);
			products.setResizable(false);
			products.setScene(scene);
			Main.setIcon(products);
			products.show();
			closeWindow(event);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	//this is used for clientside, temporar solution only !!!
	public static String ClientID;
	public static String getClientID() {
		return ClientID;
	}
	public static String setClientID(String inp) {
		return ClientID = inp;
	}
	
}
