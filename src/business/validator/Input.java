package business.validator;

import com.jfoenix.controls.JFXTextField;

import javafx.application.Platform;

/**
 * Class used to validate the input provided by the administration.
 * Used when adding/modifying an existing client/product
 * 
 * @author Pirosca Calin Lucian
 *
 */

public class Input {

	/**
	 * 
	 * A method used only for clients that receives the input field and a boolean variable that says whether
	 * the input should consists only of digits or only of letters.
	 * 
	 * @param input		The input field whose text will be extracted in order to be validated
	 * @param name		Boolean used to check whether the should be only letters or only digits
	 * @return			<code>true</code> if the input is valid
	 * 					<code>false</code> if the input is not valid
	 */
	
	public static boolean validate(JFXTextField input, boolean name) { // used for clients
		if (name == true) {
			if (!input.getText().matches("[a-zA-Z ]+")) {
				input.setText("");
				Platform.runLater(() -> input.setPromptText("Only letters allowed"));

				return false;
			} else {
				return true;
			}
		} else {
			if (!input.getText().matches("[0-9]+") || input.getText().length() != 13) {
				input.setText("");
				Platform.runLater(() -> input.setPromptText("Only last 13 digits of CNP are allowed"));
				return false;
			} else {
				return true;
			}
		}
	}

	/**
	 * 
	 * A method used only for products that receives the input field to be checked
	 * 
	 * @param input		The input field whose text will be extracted in order to be validated
	 * @return			<code>true</code> if the input is valid
	 * 					<code>false</code> if the input is not valid
	 */
	
	public static boolean validation(JFXTextField input) { // used for products
		if (!input.getText().matches("[0-9]+")) {
			input.setText("");
			Platform.runLater(() -> input.setPromptText("Only digits !"));
			return false;
		} else {
			return true;
		}
	}
	
}
