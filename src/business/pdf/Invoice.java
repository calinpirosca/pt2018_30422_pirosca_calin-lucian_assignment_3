package business.pdf;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;

import database.Connector;
import model.Bill;
import model.Order;
import presentation.MainController;

/**
 * Class used to print the bills provided by the application in a .pdf format
 * 
 * @author Pirosca Calin Lucian
 * 
 */

public class Invoice {

	
	
	private static String dest = "C:\\Users\\Work\\Desktop\\";
	private static final String img = "C:\\Users\\Work\\Desktop\\Logo.png";
	
	/**
	 * This method is used to save the information provided by the application
	 * in a .pdf format.
	 * 
	 * @param currentBill	A list that consists of information related to the products, quantity and price/unit
	 * @param invoiceNo		The number of this invoice
	 * @param date			The date when this invoice was issued
	 */
	
	public static void writeInvoice(List<Bill> currentBill, String invoiceNo, String date) {
		dest = dest+invoiceNo+".pdf";
		Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);
		Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.ITALIC);
		Font normalFontBold = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
		
		
		try {
			Document document = new Document(PageSize.A4, 50, 50, 50, 50);
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(dest));
			document.open();
			
			PdfContentByte canvas = writer.getDirectContent();
			
			Image image = Image.getInstance(img);
			image.setAbsolutePosition(40, 750);
			
			canvas.addImage(image); // adding logo of the company
			
			Phrase phrase = new Phrase("INVOICE", boldFont); // adding details of the pdf/bill
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 360, 780, 0);
			phrase = new Phrase("Invoice #", normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 360, 740, 0);
			phrase = new Phrase("Invoice Date", normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 360, 720, 0);
			phrase = new Phrase("Invoice Amount", normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 360, 700, 0);
			
			
			
			phrase = new Phrase(invoiceNo, normalFontBold); // get OrderID
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 420, 740, 0);
			phrase = new Phrase(date, normalFontBold); // get OrderDate
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 440, 720, 0);
			//phrase = new Phrase("$360.00 (USD)", normalFontBold); // get OrderTotal  UPDATED AT THE BOTTOM
			//ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 460, 700, 0);
			
			
			phrase = new Phrase("Budget", normalFont); // selling company details !
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 40, 650, 0);
			phrase = new Phrase("340 Lemon Ave #1537", normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 40, 630, 0);
			phrase = new Phrase("Walnut, California 91789", normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 40, 610, 0);
			phrase = new Phrase("United States", normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 40, 590, 0);
			
			
			boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
			normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 12);
			normalFontBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			
			// underline the selling company
			CMYKColor magentaColor = new CMYKColor(55,55,55,105);
			canvas.setColorStroke(magentaColor);
			canvas.moveTo(40, 575);
			canvas.lineTo(120, 575);
			canvas.closePathStroke();
			
			
			//Billing to
			phrase = new Phrase("BILLED TO", normalFontBold);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 40, 530, 0);
			phrase = new Phrase(MainController.getClientID(), normalFont);  // GET CLIENT ADDRESS
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 40, 510, 0);
			phrase = new Phrase("Walnut, California 91789", normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 40, 490, 0);
			phrase = new Phrase("United States", normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 40, 470, 0);
			
			//Shipping to
			phrase = new Phrase("SHIPPED TO", normalFontBold);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 240, 530, 0);
			phrase = new Phrase("340 Lemon Ave #1537", normalFont);  // GET CLIENT ADDRESS
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 240, 510, 0);
			phrase = new Phrase("Walnut, California 91789", normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 240, 490, 0);
			phrase = new Phrase("United States", normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 240, 470, 0);
			
			//separating company info from products info
			canvas.moveTo(40, 425);
			canvas.lineTo(555, 425);
			canvas.closePathStroke();
			
			
			//description units price unit amount
			phrase = new Phrase("Description", normalFontBold);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 40, 400, 0);
			phrase = new Phrase("Units", normalFontBold);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 320, 400, 0);
			phrase = new Phrase("Price/Unit", normalFontBold);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 390, 400, 0);
			phrase = new Phrase("Amount (USD)", normalFontBold);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 480, 400, 0);
			
			//another separating line
			canvas.moveTo(40, 380);
			canvas.lineTo(555, 380);
			canvas.closePathStroke();
			
			int i = 0,posX=320,total = 0;
			for(i = 0; i <currentBill.size();i++) { // getting products here to be printed
				phrase = new Phrase(currentBill.get(i).getDescription(), normalFontBold);
				ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 40, posX, 0);
				phrase = new Phrase(currentBill.get(i).getUnits(), normalFont);
				ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 330, posX, 0);
				phrase = new Phrase(currentBill.get(i).getPricePerUnit(), normalFont);
				ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 410, posX, 0);
				phrase = new Phrase(currentBill.get(i).getTotal(), normalFontBold);
				total += Integer.valueOf(currentBill.get(i).getTotal());
				ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 500, posX, 0);
				if(posX - 50 < 20) {
					document.newPage();
					posX = 800;
				}else {
					canvas.moveTo(40, posX-25);
					canvas.lineTo(555, posX-25);
					canvas.closePathStroke();
				}
				posX = posX - 50;
			}
			
			if(posX - 100 < 0) { // if there isn't enough space at the bottom of the page to write sub total and TVA make a new page
				document.newPage();
				posX = 800;
			}
			
			
			phrase = new Phrase("Sub total", normalFont); 
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 400, posX, 0);
			phrase = new Phrase(String.valueOf(total), normalFont);// GET subtotal TO BE IMPLEMENTED
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 500, posX, 0);
			phrase = new Phrase("General Tax @ 20%", normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 350, posX-30, 0);
			phrase = new Phrase(String.valueOf(total * 0.2).substring(0, String.valueOf(total * 0.2).indexOf(".")+2), normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 500, posX-30, 0);
			
			//separating line between subtotal tva and total
			canvas.moveTo(380, posX-45);
			canvas.lineTo(555, posX-45);
			canvas.closePathStroke();
			
			//ADDING TOTAL AT THE BOTTOM
			phrase = new Phrase("Total", normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 400, posX-70, 0);
			phrase = new Phrase(String.valueOf(total + total * 0.2).substring(0, String.valueOf(total + total * 0.2).indexOf(".")+2), normalFont);
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 500, posX-70, 0);
			
			// get OrderTotal AT THE TOP OF THE PAGE
			phrase = new Phrase("$"+String.valueOf(total + total * 0.2).substring(0, String.valueOf(total + total * 0.2).indexOf(".")+2), normalFontBold); 
			ColumnText.showTextAligned(canvas, Element.ALIGN_TOP, phrase, 460, 700, 0);
			
			Connector.updateObject(new Order(invoiceNo, date, MainController.getClientID(), (int)(total + total * 0.2)));
			
			document.close();
			
		} catch (DocumentException | IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
}
