package business;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

/**
 * Class used to get the corresponding FXML file.
 * Applying the factory pattern.
 * 
 * @author Pirosca Calin Lucian
 *
 */

public class FXMLFactory {
	//FXMLLoader.load(getClass().getResource())
	
	private static String addClient = "/view/fxml/client/addClient.fxml";
	private static String editClient = "/view/fxml/client/editClient.fxml";
	private static String searchClient = "/view/fxml/client/searchClient.fxml";
	private static String deleteClient = "/view/fxml/client/deleteClient.fxml";
	private static String addProduct = "/view/fxml/product/addProduct.fxml";
	private static String editProduct = "/view/fxml/product/editProduct.fxml";
	private static String searchProduct = "/view/fxml/product/searchProduct.fxml";
	private static String deleteProduct = "/view/fxml/product/deleteProduct.fxml";
	private static String clientSide = "/view/fxml/ClientSideSucces.fxml";
	
	/**
	 * 
	 * Method that returns a specific FXML based on the string input
	 * 
	 * @param FXMLtype		The string that specifies which FXML to return
	 * @return				This method returns a node, a FXMLloader that loads the corresponding FXML file
	 */
	
	public static Node getFXML(String FXMLtype) {
		
		
		if(FXMLtype == null) {
			return null;
		}
		if(FXMLtype.equalsIgnoreCase("addclient")) {
			try {
				return FXMLLoader.load(FXMLFactory.class.getResource(addClient));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(FXMLtype.equalsIgnoreCase("addproduct")) {
			try {
				return FXMLLoader.load(FXMLFactory.class.getResource(addProduct));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(FXMLtype.equalsIgnoreCase("editclient")) {
			try {
				return FXMLLoader.load(FXMLFactory.class.getResource(editClient));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(FXMLtype.equalsIgnoreCase("editproduct")) {
			try {
				return FXMLLoader.load(FXMLFactory.class.getResource(editProduct));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(FXMLtype.equalsIgnoreCase("searchclient")) {
			try {
				return FXMLLoader.load(FXMLFactory.class.getResource(searchClient));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(FXMLtype.equalsIgnoreCase("searchproduct")) {
			try {
				return FXMLLoader.load(FXMLFactory.class.getResource(searchProduct));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(FXMLtype.equalsIgnoreCase("deleteclient")) {
			try {
				return FXMLLoader.load(FXMLFactory.class.getResource(deleteClient));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(FXMLtype.equalsIgnoreCase("deleteproduct")) {
			try {
				return FXMLLoader.load(FXMLFactory.class.getResource(deleteProduct));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(FXMLtype.equalsIgnoreCase("clientside")) {
			try {
				return FXMLLoader.load(FXMLFactory.class.getResource(clientSide));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
}
