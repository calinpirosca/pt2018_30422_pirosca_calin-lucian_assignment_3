package database;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class used to establish connection to the database and the queries 
 * 
 * @author Pirosca Calin Lucian
 *
 */

public class Connector {
	private static String connectionString = "jdbc:mysql://localhost:3306/Budget";
	private static PreparedStatement statement; // using prepared statement to avoid hacking through injection
	private static ResultSet data;
	private static String user = "root";
	private static String password = "";
	private static Connection connection;

	/**
	 * 
	 * Used to initialize the connection at the very first call of any given class of this class.
	 * 
	 */
	static { // used to initialize at the first call of Connector
		try {
			connection = DriverManager.getConnection(connectionString, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * Class used to close the connection to the database at the end.
	 * 
	 */
	public static void closeConnection() {
		try {
			statement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * A method that receives an object and a flag.
	 * Using PrepartedStatement this method gets the fields of this object and sets the value corresponding to each field inside the statement
	 * The flag is used to determine how are the values set to each field inside the statement
	 * 
	 * @param object	The object whose fields and values are extracted and then those values set inside the statement to be used for the query
	 * @param flag		An integer which can be 0, for insert, 1, for update ( having pk twice, once at the end so that to know which to update )
	 * 					2, for search, to add wildcard to each value, such that it'll list all elements starting with the given values.
	 * 
	 * @see insertObject(object)
	 * @see updateObject(object)
	 */
	
	private static void setStatementParameters(Object object, int flag) { // 0 = default insert, 1 = update, pk value at the end, 2 = search, adding wildcard
		Field[] fields = object.getClass().getDeclaredFields();
		int parameterIndex = 1;
		for (Field temp : fields) {
			temp.setAccessible(true);
			try {
				if (temp.getType().toString().contains("String")) {
					if(flag == 0 || (flag == 1 && parameterIndex > 1)) {
						statement.setString(parameterIndex, temp.get(object).toString());
					}else if(flag == 2) {
						statement.setString(parameterIndex, temp.get(object).toString() + "%");
					}else if (parameterIndex == 1 && flag == 1) {
						statement.setString(parameterIndex, temp.get(object).toString());
						statement.setString(fields.length + 1, temp.get(object).toString());
					}
				} else if (temp.getType().toString().contains("int")) {
					statement.setInt(parameterIndex, Integer.valueOf(temp.get(object).toString()));
					if (parameterIndex == 1 && flag == 1) {
						statement.setInt(fields.length + 1, Integer.valueOf(temp.get(object).toString()));
					}
				}
			} catch (IllegalArgumentException | IllegalAccessException | SQLException e) {
				e.printStackTrace();
			}
			parameterIndex++;
		}
	}

	/**
	 * 
	 * A method that builds an insert query based on the fields extracted from the object.
	 * 
	 * An important thing is the fact that each object has a corresponding table in the database
	 * and for each object field there is a a column inside the corresponding table in the database.
	 * The name of the object class corresponds to name of a table, and names of an object's fields correspond
	 * to specific columns inside that corresponding table.
	 * 
	 * 
	 * @param object	The object based on which the table and the columns are determined
	 * @return			Returns the query as a String
	 * 
	 * 
	 * @see insertObject(object)
	 * @see updateObject(object)
	 * @see setStatementParameters(object, int)
	 */
	
	private static String buildInsertQuery(Object object) {
		StringBuilder sBuilder = new StringBuilder();

		sBuilder.append("INSERT INTO `");
		sBuilder.append(object.getClass().getSimpleName());
		sBuilder.append("` (");
		Field[] fields = object.getClass().getDeclaredFields();
		for (Field temp : fields) {
			temp.setAccessible(true);
			sBuilder.append(temp.getName());
			sBuilder.append(" , ");
		}
		sBuilder.replace(sBuilder.length() - 3, sBuilder.length(), ") VALUES (");
		for (int i = 0; i < fields.length; i++) {
			sBuilder.append("?, ");
		}
		sBuilder.replace(sBuilder.length() - 2, sBuilder.length(), ")");
		
		return sBuilder.toString();
	}

	/**
	 * 
	 * A method that builds an update query based on the fields extracted from the object.
	 * 
	 * An important thing is the fact that each object has a corresponding table in the database
	 * and for each object field there is a a column inside the corresponding table in the database.
	 * The name of the object class corresponds to name of a table, and names of an object's fields correspond
	 * to specific columns inside that corresponding table.
	 * 
	 * 
	 * @param object	The object based on which the table and the columns are determined
	 * @return			Returns the query as a String
	 * 
	 * 
	 * @see insertObject(object)
	 * @see updateObject(object)
	 * @see setStatementParameters(object, int)
	 */
	
	private static String buildUpdateQuery(Object object) { // update by pk
		StringBuilder sBuilder = new StringBuilder();

		sBuilder.append("UPDATE `");
		sBuilder.append(object.getClass().getSimpleName());
		sBuilder.append("` SET ");
		Field[] fields = object.getClass().getDeclaredFields();
		for (Field temp : fields) {
			temp.setAccessible(true);
			sBuilder.append(temp.getName());
			sBuilder.append(" = ?, ");
		}
		sBuilder.replace(sBuilder.length() - 2, sBuilder.length(), " WHERE ");
		sBuilder.append(fields[0].getName().toString()); // this is the pk
		sBuilder.append(" = ?");

		return sBuilder.toString();
	}
	
	/**
	 * 
	 * A method that calls appropriate methods in order to build an insert query appropriate for the given object
	 * to set the values for that statement, and then to actually insert it into the database
	 * 
	 * @param object	The object that determines which table to be used when inserting, it will also specifies the columns based on
	 * 					its fields and the values to be inserted
	 * @return			<code>true</code> if it was successfully inserted
	 * 					<code>false</code> if it couldn't be inserted
	 * 
	 * @see buildInsertQuery(object)
	 * @see setStatementParameters(object, int)
	 */

	public static boolean insertObject(Object object) {
		try {
			String insert = buildInsertQuery(object);
			statement = connection.prepareStatement(insert);
			setStatementParameters(object, 0);
			statement.executeUpdate();
			//closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	/**
	 * 
	 * A method that calls appropriate methods in order to build an update query appropriate for the given object
	 * to set the values for that statement, and then to actually insert it into the database
	 * 
	 * @param object	The object that determines which table to be used when update, it will also specifies the columns based on
	 * 					its fields and the values to be inserted
	 * @return			<code>true</code> if it was successfully update
	 * 					<code>false</code> if it couldn't be update
	 * 
	 * @see buildUpdateQuery(object)
	 * @see setStatementParameters(object, int)
	 */
	
	public static boolean updateObject(Object object) { // This method works by updating based on the PK !
		try {
			System.out.println(object.toString());
			statement = connection.prepareStatement(buildUpdateQuery(object));
			setStatementParameters(object, 1);
			statement.executeUpdate();
			//closeConnection();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}
	
	private static String buildSearchQuery(Object object) {
		StringBuilder sBuilder = new StringBuilder();

		sBuilder.append("SELECT * FROM `");
		sBuilder.append(object.getClass().getSimpleName());
		sBuilder.append("` WHERE ");
		Field[] fields = object.getClass().getDeclaredFields();
		for (Field temp : fields) {
			temp.setAccessible(true);
			sBuilder.append(temp.getName());
			sBuilder.append(" LIKE ? AND ");
		}
		sBuilder.replace(sBuilder.length() - 5, sBuilder.length(), "");
		
		return sBuilder.toString();
	}
	
	/**
	 * 
	 * A method that calls appropriate methods in order to build a search query appropriate for the given object
	 * to set the values for that statement, and then to actually insert it into the database
	 * 
	 * @param object	The object that determines which table to be used when update, it will also specifies the columns based on
	 * 					its fields and the values to be inserted
	 * @return			It returns a ResultSet consisting of elements from the database based on the values provided by the object's values 
	 * 					plus a wildcard at the end of each value
	 * 
	 * @see buildUpdateQuery(object)
	 * @see setStatementParameters(object, int)
	 */
	
	public static ResultSet searchObject(Object object) {
		try {
			statement = connection.prepareStatement(buildSearchQuery(object));
			setStatementParameters(object, 2);
			data = statement.executeQuery();
			//closeConnection();
			return data;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	/**
	 * 
	 * A method that finds an element based on the given parameters
	 * 
	 * @param id		It specifies the value to be searched
	 * @param table		It specifies in which table to look for the element
	 * @param pk		It specifies which column to be used when searching
	 * 
	 * @return			It returns the element if there is any found or null otherwise
	 */
	
	public static ResultSet findByID(String id, String table, String pk) {
		String insert = "SELECT * FROM `" + table + "` WHERE " + pk + " = ?";
		try {
			statement = connection.prepareStatement(insert);
			statement.setString(1, id);
			data = statement.executeQuery();
			//closeConnection();
			if (data.next()) {
				return data;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}
		return null;
	}

	/**
	 * 
	 * A method that deletes an element based on the given parameters
	 * 
	 * @param id		It specifies the value to be deleted
	 * @param table		It specifies in which table to look for the element
	 * @param pk		It specifies which column to be used when deleting
	 * 
	 * @return			<code>true</code> if it is successfully deleted
	 * 					<code>false</code> if it couldn't be deleted
	 */
	
	public static boolean deleteByID(String id, String table, String pk) {
		String insert = "DELETE FROM `" + table + "` WHERE " + pk + " = ?";
		try {
			statement = connection.prepareStatement(insert);
			statement.setString(1, id);
			statement.executeUpdate();
			//closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
