package database;

import java.beans.IntrospectionException;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
@SuppressWarnings({ "unchecked", "rawtypes" })
/**
 * Class used to display all the elements in a table
 * The header of the table is determined using reflection.
 * 
 * @author Pirosca Calin Lucian
 *
 */
public class Search { // used to populate a table

	public static ResultSet data;
	private Class<?> clasa;
	private Object instance;
	
	/**
	 * 
	 * Method used to get the header of the table based on the provided ResultSet
	 * and add those columns to the given TableView
	 * 
	 * @param data		The given ResultSet based on which the header of the table is determined
	 * @param table		The table where the columns are added 
	 * 
	 */
	private void setCol(ResultSet data,TableView<?> table) {
		try {
			if(data.next()) {
				ResultSetMetaData meta = data.getMetaData();
				for(int i =1 ;i<=meta.getColumnCount();i++) {
					TableColumn column = new TableColumn<>(meta.getColumnName(i));
					column.setCellValueFactory(new PropertyValueFactory<>(meta.getColumnName(i)));
					table.getColumns().add(column);
				}
				data.first();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * A method that creates the header of the table and then returns a list of elements
	 * extracted from the given ResultSet which is used by the calling method
	 * to populate the table using the provided list
	 * 
	 * @param data		Where the elements are extracted from
	 * @param table		Where the elements are added
	 * @return			Returns a list of objects converted from the ResultSet
	 * 
	 * @see setCol(ResultSet, TableView<?>)
	 */
	public List populate(ResultSet data, TableView<?> table) { // REFLECTION USED
		List obiecte = new ArrayList<>();
		setCol(data, table);
		try {
			data.beforeFirst();
			while(data.next()) {
				ResultSetMetaData meta = data.getMetaData();
				String getClasa = meta.getTableName(1);
				getClasa = getClasa.substring(0, 1).toUpperCase() + getClasa.substring(1);
				getClasa = "model."+getClasa;
				clasa = Class.forName(getClasa);
				instance = clasa.newInstance();
				for(Field field : clasa.getDeclaredFields()) {
					Object value = data.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), clasa);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value.toString());
				}
				obiecte.add(instance);
			}
		} catch (SQLException | SecurityException | IllegalArgumentException | IntrospectionException | IllegalAccessException | InvocationTargetException | ClassNotFoundException | InstantiationException e) {
			System.out.println(e.getMessage());
			//e.printStackTrace();
		}
		return obiecte;
	}
}
